;;; Guile-Amb --- Ambiguous operator for Guile
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Guile-Amb.
;;;
;;; Guile-Amb is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile-Amb is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Amb.  If not, see <http://www.gnu.org/licenses/>.

(define-module (amb)
  #:use-module (ice-9 control)
  #:use-module (ice-9 exceptions)
  #:use-module ((srfi srfi-1) #:select (reverse!))
  #:export (amb-prompt-tag
            call-with-amb-prompt
            amb-fail?
            amb
            amb-assert
            amb-find
            amb-collect
            amb-fold
            amb-count
            amb-from
            amb-iota
            amb-choose
            amb-iterate
            amb-take
            amb-drop
            amb-take-while
            amb-drop-while
            amb-map))

(define amb-prompt-tag (make-parameter (make-prompt-tag 'amb)))

(define-exception-type &amb-fail
  &exception
  make-amb-fail
  amb-fail?)

(define* (call-with-amb-prompt thunk
                               #:optional
                               (failure-continuation
                                (lambda ()
                                  (raise-continuable
                                   (make-exception
                                    (make-amb-fail)
                                    (make-exception-with-message
                                     "Amb tree exhausted"))))))
  (call-with-prompt (amb-prompt-tag)
    thunk
    (lambda (k handler) (handler k failure-continuation))))

(define-syntax amb
  (syntax-rules ()
    ((_ x y ...)
     (abort-to-prompt (amb-prompt-tag)
       (lambda (k fail)
         (call-with-amb-prompt
           (lambda () (k x))
           (lambda ()
             (call-with-amb-prompt
               (lambda () (k (amb y ...)))
               fail))))))
    ((_)
     (abort-to-prompt (amb-prompt-tag)
       (lambda (k fail)
         (fail))))))

(define (amb-assert pred)
  (or pred (amb)))

(define-syntax-rule (amb-find expr)
  (amb-find/thunk (lambda () expr)))

(define (amb-find/thunk thunk)
  (call-with-amb-prompt
    thunk
    (lambda () #f)))

(define-syntax-rule (amb-collect expr)
  (amb-collect/thunk (lambda () expr)))

(define (amb-collect/thunk thunk)
  (let ((result '()))
    (call-with-amb-prompt
      (lambda ()
        (set! result (cons (thunk) result))
        (amb))
      (lambda ()
        (reverse! result)))))

(define-syntax-rule (amb-count expr)
  (amb-count/thunk (lambda () expr)))

(define (amb-count/thunk thunk)
  (let ((result 0))
    (call-with-amb-prompt
      (lambda ()
        (thunk)
        (set! result (+ result 1))
        (amb))
      (lambda () result))))

(define-syntax-rule (amb-fold proc init expr)
  (amb-fold/thunk proc init (lambda () expr)))

(define (amb-fold/thunk proc init thunk)
  (let ((result init))
    (call-with-amb-prompt
      (lambda ()
        (set! result (proc (thunk) init))
        (amb))
      (lambda () result))))

(define* (amb-from start #:optional (step 1))
  (amb start (amb-from (+ start step) step)))

(define* (amb-iota count #:optional (start 0) (step 1))
  (if (zero? count)
      (amb)
      (amb start (amb-iota (- count 1) (+ start step) step))))

(define (amb-choose lst)
  (if (null? lst)
      (amb)
      (amb (car lst) (amb-choose (cdr lst)))))

(define (amb-iterate proc base)
  (amb base (amb-iterate proc (proc base))))

(define-syntax-rule (amb-take n expr)
  (amb-take/thunk n (lambda () expr)))

(define (amb-take/thunk n thunk)
  (if (zero? n)
      (amb)
      (abort-to-prompt (amb-prompt-tag)
        (lambda (k top-fail)
          (call-with-amb-prompt
            (lambda ()
              (let ((result (thunk)))
                (set! n (- n 1))
                (call-with-prompt (amb-prompt-tag)
                  (lambda () (k result))
                  (lambda (k2 handler)
                    (if (zero? n)
                        (handler k2 top-fail)
                        (abort-to-prompt (amb-prompt-tag)
                          (lambda (k3 fail)
                            (handler k2 fail))))))))
            top-fail)))))

(define-syntax-rule (amb-drop n expr)
  (amb-drop/thunk n (lambda () expr)))

(define (amb-drop/thunk n thunk)
  (if (zero? n)
      (thunk)
      (let ((return? #f))
        (let ((result (thunk)))
          (cond
           (return? result)
           (else
            (set! n (- n 1))
            (when (zero? n)
              (set! return? #t))
            (amb)))))))

(define-syntax-rule (amb-take-while proc expr)
  (amb-take-while/thunk proc (lambda () expr)))

(define (amb-take-while/thunk proc thunk)
  (abort-to-prompt (amb-prompt-tag)
    (lambda (k top-fail)
      (call-with-amb-prompt
        (lambda ()
          (let ((result (thunk)))
            (call-with-prompt (amb-prompt-tag)
              (lambda ()
                (if (proc result)
                    (k result)
                    (top-fail)))
              (lambda (k2 handler)
                (abort-to-prompt (amb-prompt-tag)
                  (lambda (k3 fail)
                    (handler k2 fail)))))))
        top-fail))))

(define-syntax-rule (amb-drop-while proc expr)
  (amb-drop-while/thunk proc (lambda () expr)))

(define (amb-drop-while/thunk proc thunk)
  (let ((return? #f))
    (let ((result (thunk)))
      (cond
       (return? result)
       ((proc result) (amb))
       (else
        (set! return? #t)
        result)))))

(define-syntax-rule (amb-map proc expr more-expr ...)
  (amb-map/thunk proc (lambda () expr) (lambda () more-expr) ...))

(define* (amb-map/thunk proc thunk #:rest thunks)
  (let ((gens (map generator-amb-collect/thunk (cons thunk thunks))))
    (let loop ()
      (let ((args (map-in-order (lambda (gen)
                                  (let ((x (gen)))
                                    (if (end-of-generator? x)
                                        (amb)
                                        x)))
                                gens)))
        (amb (apply proc args) (loop))))))

(define end-of-generator (list 'end-of-generator))

(define (end-of-generator? x)
  (eq? x end-of-generator))

(define (make-generator proc)
  (let* ((next #f)
         (prompt-tag (make-prompt-tag 'generator))
         (yield (lambda (x)
                  (abort-to-prompt prompt-tag x))))
    (set! next
      (lambda ()
        (let loop ((k0 (lambda (v)
                         (proc yield)
                         end-of-generator)))
          (call-with-prompt prompt-tag
            (lambda () (k0 #f))
            (lambda (k value)
              (set! next (lambda () (loop k)))
              value)))))
    (lambda () (next))))

(define-syntax-rule (generator-amb-collect expr)
  (generator-amb-collect/thunk (lambda () expr)))

(define (generator-amb-collect/thunk thunk)
  (make-generator
   (lambda (yield)
     (call-with-amb-prompt
       (lambda ()
         (let ((x (thunk)))
           (yield x))
         (amb))
       (lambda () end-of-generator)))))

(define (generator-amb-choose gen)
  (let ((x (gen)))
    (if (end-of-generator? x)
        (amb)
        (amb x (generator-amb-choose gen)))))
