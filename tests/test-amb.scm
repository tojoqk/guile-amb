;;; Guile-Amb --- Ambiguous operator for Guile
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Guile-Amb.
;;;
;;; Guile-Amb is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile-Amb is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Amb.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-amb)
  #:use-module (amb)
  #:use-module (srfi srfi-64))

(test-begin "amb")

(test-equal "call-with-amb-prompt"
  (list 2 10)
  (call-with-amb-prompt
    (lambda ()
      (let ((a (amb 1 2 3))
            (b (amb 10 20 30)))
        (if (even? (+ a b))
            (list a b)
            (amb))))))

(test-end "amb")
