;;; Guile-Amb --- Ambiguous operator for Guile
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of Guile-Amb.
;;;
;;; Guile-Amb is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Guile-Amb is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-Amb.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-8-queens)
  #:use-module (amb)
  #:use-module (srfi srfi-64))

(define (pos i j)
  (list i j))

(define (pos-i p)
  (car p))

(define (pos-j p)
  (cadr p))

(define (attacking? p q)
  (or (= (pos-i p) (pos-i q))
      (= (pos-j p) (pos-j q))
      (= (+ (pos-i p) (pos-j p))
         (+ (pos-i q) (pos-j q)))
      (= (- (pos-i p) (pos-j p))
         (- (pos-i q) (pos-j q)))))

(define (n-queens n)
  (define (assert-safe p qs)
    (for-each (lambda (q)
                (amb-assert (not (attacking? p q))))
              qs))
  (do ((j 0 (+ j 1))
       (qs '() (let* ((i (amb-iota n))
                      (p (pos i j)))
                 (assert-safe p qs)
                 (cons p qs))))
      ((= j n) qs)))

(test-begin "8-queens")

(test-eqv "number of solutions"
  92
  (amb-count (n-queens 8)))

(test-end "8-queens")
