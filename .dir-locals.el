;; Per-directory local variables for GNU Emacs 23 and later.

((scheme-mode
  .
  ((indent-tabs-mode . nil)
   (eval . (put 'call-with-amb-prompt 'scheme-indent-function 0))
   (eval . (put 'call-with-prompt 'scheme-indent-function 1))
   (eval . (put 'abort-to-prompt 'scheme-indent-function 1))
   (eval . (put 'stream-let 'scheme-indent-function 2)))))
